<?php
require_once 'classes/auth.class.php';
require_once 'classes/responses.class.php';

//Cabeceras HTTP en PHP para permitir el acceso CORS
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if($method == "OPTIONS") {
    die();
}


$_auth = new auth;
$_responses = new responses;  //se instancia la clase

if($_SERVER['REQUEST_METHOD'] == 'POST'){ //solo vamos a permitir acceder desde el metodo post ((ver en postman))
    //Recibir datos en JSON del front o postman
    $postBody = file_get_contents("php://input");
    //print_r($postBody);

    //Enviamos datos al manejador auth.class
    $Arraydata = $_auth->login($postBody);


    //devolvemos una respuesta
    header('Content-Type: application/json');
    if(isset($Arraydata["result"]["error_id"])){
        $responseCode = $Arraydata["result"]["error_id"];
        http_response_code($responseCode);
    }else{
        http_response_code(200);
    }
    //print_r(json_encode($Arraydata));
    echo json_encode($Arraydata);

}else{
    //echo "Metodo no permitido";
    header('Content-Type: application/json');
    $Arraydata = $_responses->error_405();
    echo json_encode($Arraydata);
}





?>