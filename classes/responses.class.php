<?php

class responses{

    public $response = [
        'status' => "ok",
        'result' => array()
    ];

    public function error_405(){
        $this->response['status'] = "error"; //cambia el atributo del array de ok a error
        $this->response['result'] = array(
            "error_id" => "405",
            "error_msg" => "Metodo no permitido"
        );
        return $this->response;
    }
    //parametro de entrada con valor es que puede tener el parametro pero es opcional
    public function error_200($string = "Datos incorrectos"){ //codigo 200 es "OK" pero en este caso es cuando el api recibe la solicitud pero hay un error con ella ((error 200 no existe realmente))
        $this->response['status'] = "error"; //cambia el atributo del array de ok a error
        $this->response['result'] = array(
            "error_id" => "200",
            "error_msg" => $string
        );
        return $this->response;
    }

    public function error_400(){ 
        $this->response['status'] = "error"; //cambia el atributo del array de ok a error
        $this->response['result'] = array(
            "error_id" => "400",
            "error_msg" => "Datos enviados incompletos o con formato incorrecto"
        );
        return $this->response;
    }

    public function error_500($string = "Error interno del servidor"){ //codigo 200 es "OK" pero en este caso es cuando el api recibe la solicitud pero hay un error con ella ((error 200 no existe realmente))
        $this->response['status'] = "error"; //cambia el atributo del array de ok a error
        $this->response['result'] = array(
            "error_id" => "500",
            "error_msg" => $string
        );
        return $this->response;
    }

    public function error_401($string = "No autorizado"){ //codigo 200 es "OK" pero en este caso es cuando el api recibe la solicitud pero hay un error con ella ((error 200 no existe realmente))
        $this->response['status'] = "error"; //cambia el atributo del array de ok a error
        $this->response['result'] = array(
            "error_id" => "401",
            "error_msg" => $string
        );
        return $this->response;
    }

}


?>