<?php
require_once 'connection/connection.php';
require_once 'responses.class.php';

class auth extends connection{

    public function login($json){
        $_responses = new responses;
        $data = json_decode($json, true);
        if(!isset($data['usuario']) || !isset($data['password'])){
            //error en los campos
            return $_responses->error_400();
        }else{
            //todo ok debe recibir estos datos por json desde el frontend o postman en un JSON por el metodo POST
            $usuario = $data['usuario']; //en este caso en el json sera el correo por ahora...
            $password = $data['password'];
            $password = parent::encrypt($password);
            $data = $this->getUserData($usuario);
            if($data){
                //si existe usuario en la db verifico el password
                if($password==$data[0]["Password"]){
                    if($data[0]["Estado"]=="activo"){
                        //crear el token
                        //$verify = $this->TokenInsert($data[0]["Correo"]);
                        $verify = $this->TokenInsert($usuario);
                        if($verify){
                            // si se guardo, debemos maketarlo ->
                            $result = $_responses->response;
                            $result["result"] = array(
                                "token" =>$verify,
                                "userid" =>$data[0]["UserId"]
                            );
                            return $result;
                        }else{
                            //error al guardar
                            return $_responses->error_500("Error interno, no hemos podido guardar");
                        }
                    }else{
                        //El usuario esta inactivo
                        return $_responses->error_200("El usuario esta inactivo");
                    }

                }else{
                    //La contraseña no es igual
                    return $_responses->error_200("Password invalido");
                }
                
            }else{
                //no existe usuario en la db por que devolvio 0 en la funcion getUserdata
                return $_responses->error_200("El usuario $usuario no existe");
            }
        }
    }


    private function getUserData($mail){
        $query = "SELECT UserId, Password, Estado FROM usuarios WHERE Correo = '$mail'";
        $data = parent::getData($query);
        if(isset($data[0]["UserId"])){
            return $data;
        }else{
            return 0;
        }
    }

    private function TokenInsert($UsuarioId){
        $val = true;
        //valores aleatorios
        $token = bin2hex(openssl_random_pseudo_bytes(16,$val));
        $date = date("Y-m-d H:i");
        $estado = "activo";
        $query = "INSERT INTO usuarios_token (UsuarioID,Token,Estado,Fecha) VALUES('$UsuarioId','$token','$estado','$date')";
        $verify = parent::nonQuery($query);
        if($verify){
            return $token;
        }else{
            return 0;
        }
    }



}

?>