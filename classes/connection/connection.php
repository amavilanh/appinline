<?php
class connection{
    

    private $server;
    private $user;
    private $password;
    private $database;
    private $port;
    private $connection;

    function __construct(){   //metodo constructor se llama utomaticamente
        $datalist = $this->dataConnection();
        foreach ($datalist as $key => $value){ //key guarda dato temporal y value guarda todos los datos del array
            $this->server = $value['server'];
            $this->user = $value['user'];
            $this->password = $value['password'];
            $this->database = $value['database'];
            $this->port = $value['port'];
        }
        $this->connection = new mysqli($this->server, $this->user,$this->password,$this->database,$this->port); //clase de sql
        if($this->connection->connect_errno){
            echo "Algo va mal con la conexion";
            die();
        }
    }

    private function dataConnection(){
        $address = dirname(__FILE__);
        $jsondata = file_get_contents($address . "/" . "config");
        return json_decode($jsondata, true); //convierte en un array asociativo el true
    }

    private function UTF8Convert($array){
        array_walk_recursive($array,function(&$item,$key){
            if(!mb_detect_encoding($item, 'utf-8', true)){
                $item = utf8_encode($item);
            }
        });
        return $array;
    }

    public function getData($query){
        $results = $this -> connection->query($query); //funcion de la estancia de mysqli y parametro con el mismo nombre "query"
        $resultArray = array();
        foreach($results as $key){
            $resultArray[] = $key;
        }
        return $this -> UTF8Convert($resultArray);
    }

    public function nonQuery($query){  //metodo consulta $query es un $sqlstring
        $results = $this->connection->query($query);
        //devuelve las filas afectadas
        return $this->connection->affected_rows;
    }

    public function nonQueryId($query){  //Se usa para los INSERT... devuelve ultimo ID de la fila que insertamos
        $results = $this->connection->query($query);
        $filas = $this->connection->affected_rows;
        if($filas >=1 ){
            return $this->connection->insert_id;
        }else{
            return 0;
        }
    }

    protected function encrypt($string){
        return md5($string);
    }

}
 

?>