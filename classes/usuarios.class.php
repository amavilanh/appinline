<?php
require_once "connection/connection.php";
require_once "responses.class.php";


class usuarios extends connection {

    private $table = "usuarios";
    private $userid = "";
    private $nombre = "";
    private $nacionalidad = "";
    private $ciudad = "";
    private $genero = "";
    private $telefono = "";
    private $fechanacimiento = "0000-00-00";
    private $correo = "";
    private $password = "";
    private $estado = "";
    private $admin = "";
    private $token = "";
    public function usuarioslist($pagina = 1){
        $inicio = 0;
        $cantidad = 100;
        if($pagina > 1){
            $inicio = (($cantidad * ($pagina - 1)) +1);
            $cantidad = $cantidad * $pagina;
        }

        $query = "SELECT UserId,Nombre,Genero,Ciudad,Celular,Correo FROM " . $this->table . " limit $inicio,$cantidad";
        $data = parent::getData($query);
        return ($data);
    }

    public function getUser($id){
        $query = "SELECT UserId,Nombre,Genero,Nacionalidad,Ciudad,Celular,Fechanacimiento,Correo,Estado,Admin FROM " . $this->table . " WHERE UserId = '$id'";
        return parent::getData($query);
    }


    public function Post($json){
        $_responses = new responses;
        $data = json_decode($json,true);

        //si no existe el token en la entrada del json
        if(!isset($data['token'])){
            return $_responses->error_401();
        }else{
            //el token existe en la entrada del json
            $this->token = $data['token'];
            $arraytoken = $this->tokensearch();
            if($arraytoken){
                //el token es valido y activo y a continuacion va toda la logica del post que habiamos escrito antes del token
                if(!isset($data['nombre']) || !isset($data['correo']) || !isset($data['password'])){
                    return $_responses->error_400();
                }else{
                    $this->nombre = $data['nombre'];
                    $this->correo = $data['correo'];
                    $this->nacionalidad = $data['nacionalidad'];
                    $this->ciudad = $data['ciudad'];
                    $this->genero = $data['genero'];
                    $this->telefono = $data['telefono'];
                    $this->fechanacimiento = $data['fechanacimiento'];
                    $this->password = parent::encrypt($data['password']);
                    $this->estado = "inactivo";
                    $resp = $this->userinsert();
                    if($resp){
                        $response = $_responses->response;
                        $response["result"] = array(
                            "UsuarioId" => $resp
                        );
                        return $response;
                    }else{
                        return $_responses->error_500();
                    }
                }

            }else{
                //el token no se encuentra en la db es invalido o esta inactivo
                return $_responses->error_401("El token enviado es invalido o ha caducado");
            }
        }

    }

    public function Put($json){
        $_responses = new responses;
        $data = json_decode($json,true);

        //si no existe el token en la entrada del json
        if(!isset($data['token'])){
            return $_responses->error_401();
        }else{
            //el token existe en la entrada del json
            $this->token = $data['token'];
            $arraytoken = $this->tokensearch();
            if($arraytoken){
                //el token es valido y activo y a continuacion va toda la logica del put
                if(!isset($data['usuarioid'])){
                    return $_responses->error_400();
                }else{
                    $this->userid = $data['usuarioid'];
                    $this->nombre = $data['nombre'];
                    $this->correo = $data['correo'];
                    $this->nacionalidad = $data['nacionalidad'];
                    $this->ciudad = $data['ciudad'];
                    $this->genero = $data['genero'];
                    $this->telefono = $data['telefono'];
                    $this->fechanacimiento = $data['fechanacimiento'];
                    $this->password = parent::encrypt($data['password']);
                    $this->estado = $data['estado'];
                    $resp = $this->useredit();
                    if($resp){
                        $response = $_responses->response;
                        $response["result"] = array(
                            "UsuarioId" => $this->userid
                        );
                        return $response;
                    }else{
                        return $_responses->error_500();
                    }
                }

            }else{
                //el token no se encuentra en la db es invalido o esta inactivo
                return $_responses->error_401("El token enviado es invalido o ha caducado");
            }
        }

    }

    public function delete($json){
        $_responses = new responses;
        $data = json_decode($json,true);

        //si no existe el token en la entrada del json
        if(!isset($data['token'])){
            return $_responses->error_401();
        }else{
            //el token existe en la entrada del json
            $this->token = $data['token'];
            $arraytoken = $this->tokensearch();
            if($arraytoken){
                //el token es valido y activo y a continuacion va toda la logica del delete
                if(!isset($data['usuarioid'])){
                    return $_responses->error_400();
                }else{
                    $this->userid = $data['usuarioid'];
                    $resp = $this->userdelete();
                    if($resp){
                        $response = $_responses->response;
                        $response["result"] = array(
                            "UsuarioId" => $this->userid
                        );
                        return $response;
                    }else{
                        return $_responses->error_500();
                    }
                }

            }else{
                //el token no se encuentra en la db es invalido o esta inactivo
                return $_responses->error_401("El token enviado es invalido o ha caducado");
            }
        }

    }

    private function userinsert(){
        //en PHP 7+ no hay necesidad de concatenar con . y "" se puede poner todo dentro de las "" 
        $query = "INSERT INTO $this->table (Nombre,Correo,Nacionalidad,Ciudad,Genero,Celular,Fechanacimiento,Password,Estado) 
        VALUES ('$this->nombre','$this->correo','$this->nacionalidad','$this->ciudad','$this->genero','$this->telefono','$this->fechanacimiento','$this->password','$this->estado')";
        //print_r($query);
        $resp = parent::nonQueryId($query);
        if($resp){
            return $resp;
        }else{
            return 0;
        }
    }

    private function useredit(){
        //en PHP 7+ no hay necesidad de concatenar con . y "" se puede poner todo dentro de las "" 
        $query = "UPDATE $this->table SET Nombre='$this->nombre',Correo='$this->correo',Nacionalidad='$this->nacionalidad',Ciudad='$this->ciudad',Genero='$this->genero',Celular='$this->telefono',Fechanacimiento='$this->fechanacimiento',Password='$this->password',Estado='$this->estado' WHERE UserId = '$this->userid'";
        //print_r($query);
        $resp = parent::nonQuery($query);
        if($resp>=1){
            return $resp;
        }else{
            return 0;
        }
    }

    private function userdelete(){
        $query = "DELETE FROM $this->table WHERE UserId = '$this->userid'";
        $resp = parent::nonQuery($query);
        if($resp>=1){
            return $resp;
        }else{
            return 0;
        }
    }

    private function tokensearch(){
        $query = "SELECT Token_ID,UsuarioID,Estado FROM usuarios_token WHERE Token = '$this->token' AND Estado = 'activo'";
        $resp = parent::getData($query);
        if($resp){
            return $resp;
        }else{
            return 0;
        }
    }

    private function tokenupdate($tokenid){
        $date = date("Y-m-d H:i");
        $query = "UPDATE usuarios_token SET Fecha = '$date' WHERE Token_ID = '$tokenid'";
        $resp = parent::nonQuery($query);
        if($resp>=1){
            return $resp;
        }else{
            return 0;
        }
    }


}


//      //si no existe el token en la entrada del json
//      if(!isset($data['token'])){
//          return $_responses->error_401();
//      }else{
//          //el token existe en la entrada del json
//          $this->token = $data['token'];
//          $arraytoken = $this->tokensearch();
//          if($arraytoken){
//              //el token es valido y activo y a continuacion va toda la logica del post
//      
//          }else{
//              //el token no se encuentra en la db es invalido o esta inactivo
//              return $_responses->error_401("El token enviado es invalido o ha caducado");
//          }
//      }

?>


