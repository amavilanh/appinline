<?php
require_once 'classes/responses.class.php';
require_once 'classes/usuarios.class.php';

//Cabeceras HTTP en PHP para permitir el acceso CORS
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if($method == "OPTIONS") {
    die();
}

$_responses = new responses;
$_usuarios = new usuarios;

if($_SERVER["REQUEST_METHOD"] == "GET"){
    //se debe enviar la variable desde el navegador ../usuarios?page=1
    if(isset($_GET["page"])){
        $pagina = $_GET["page"];
        $usuarioslist = $_usuarios->usuarioslist($pagina);
        header("Content-Type: application/json");
        echo json_encode($usuarioslist);
        http_response_code(200);
    }else if(isset($_GET["id"])){
        //se debe enviar la variable desde el navegador ../usuarios?id=1
        $userid=$_GET["id"];
        $userdata = $_usuarios->getUser($userid);
        header("Content-Type: application/json");
        echo json_encode($userdata);
        http_response_code(200);
    }
    

}else if($_SERVER["REQUEST_METHOD"] == "POST"){
    //recibimos los datos enviados en la variable postbody
    $postbody = file_get_contents("php://input");
    //enviamos los datos al manejador
    $resp = $_usuarios->post($postbody);
    //print_r($resp);
    //Devolvemos una respuesta... el print de arriba solo devolvia el array con las filas afectadas y el ok
    header('Content-Type: application/json');
    if(isset($resp["result"]["error_id"])){
        $responseCode = $resp["result"]["error_id"];
        http_response_code($responseCode);
    }else{
        http_response_code(200);
    }
    echo json_encode($resp);

}else if(($_SERVER["REQUEST_METHOD"] == "PUT")){
    //recibimos los datos enviados en la variable postbody
    $postbody = file_get_contents("php://input");
    //enviamos datos al manejador
    $resp = $_usuarios->Put($postbody);
    //Devolvemos una respuesta
    header('Content-Type: application/json');
    if(isset($resp["result"]["error_id"])){
        $responseCode = $resp["result"]["error_id"];
        http_response_code($responseCode);
    }else{
        http_response_code(200);
    }
    echo json_encode($resp);
    

}else if($_SERVER["REQUEST_METHOD"] == "DELETE"){
    //recibimos los datos enviados en la variable postbody
    $postbody = file_get_contents("php://input");
    //enviamos datos al manejador
    $resp = $_usuarios->delete($postbody);
    //Devolvemos una respuesta
    header('Content-Type: application/json');
    if(isset($resp["result"]["error_id"])){
        $responseCode = $resp["result"]["error_id"];
        http_response_code($responseCode);
    }else{
        http_response_code(200);
    }
    echo json_encode($resp);

}

else{
    header('Content-Type: application/json');
    $Arraydata = $_responses->error_405();
    echo json_encode($Arraydata);
}



?>