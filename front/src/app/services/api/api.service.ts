import { Injectable } from '@angular/core';
import { LoginI } from '../../models/login.interface';
import { ResponseI } from '../../models/response.interface';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserlistI } from '../../models/userlist.interface';
import { UserI } from '../../models/user.interface';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  url:string = "http://localhost/EnLinea/";

  constructor(private http:HttpClient) { }

  //Obligamos a la funcion que retorne un objeto observable del tipo ResponseI
  //recibimos un form pero con la caracteristica de LoginI
  toLogin(form:LoginI):Observable<ResponseI>{
    let link = this.url + "auth";
    return this.http.post<ResponseI>(link,form);
  }

  isAdmin(id:number){
    let link = this.url + "usuarios?id="+id;
    return this.http.post<Boolean>(link,id);
  }

  getAllUsers(page:number):Observable<UserlistI[]>{
    let link = this.url + "usuarios?page=" + page;
    return this.http.get<UserlistI[]>(link);
  }

  getUser(id:any):Observable<UserI>{
    let link = this.url + "usuarios?id="+ id;
    return this.http.get<UserI>(link);
  }

  putUser(form:UserI):Observable<ResponseI>{
    let link = this.url + "usuarios";
    return this.http.put<ResponseI>(link,form);
  }

  deleteUser(form):Observable<ResponseI>{
    let link = this.url + "usuarios";
    let options = {
      headers: new HttpHeaders({
        'Content-type': 'application/json'
      }),
      body:form
    }
    return this.http.delete<ResponseI>(link,options);
  }

  postUser(form:UserI):Observable<ResponseI>{
    let link = this.url + "usuarios";
    return this.http.post<ResponseI>(link,form);
  }

}
