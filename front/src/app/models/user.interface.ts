export interface UserI{
    UserId:number;
    Nombre:string;
    Genero:string;
    Nacionalidad:string;
    Ciudad:string;
    Celular:string;
    Fechanacimiento:Date;
    Password:string;
    Correo:string;
    Estado:string;
    Admin:boolean;
    token:string;
}