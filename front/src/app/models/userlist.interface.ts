export interface UserlistI{
    UserId:string;
    Nombre:string;
    Genero:string;
    Ciudad:string;
    Celular:string;
    Correo:string;
}