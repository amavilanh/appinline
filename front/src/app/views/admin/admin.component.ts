import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UserlistI } from '../../models/userlist.interface';
import {  UserI } from '../../models/user.interface';
import { ResponseI } from '../../models/response.interface';
import { AlertService } from '../../services/alert/alert.service'

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit{

  usuarios:UserlistI[];
  userid:number;
  constructor(private api:ApiService, private router:Router, private alerts:AlertService, private activerouter:ActivatedRoute){}

  ngOnInit(): void{
    this.userid=Number(this.activerouter.snapshot.paramMap.get('id'));
    this.api.getAllUsers(1).subscribe(data =>{
      //console.log(data);
      this.usuarios=data;
    });

  }

  add(){
    this.router.navigate(['newuser']);
  }

  edit(id:any){
    this.router.navigate(['edit', id]);
  }

  delete(id:any){
    let query= {
      usuarioid:id,
      token:this.getToken()
    }
    this.api.deleteUser(query).subscribe(data =>{
      let response:ResponseI=data;
      if(response.status=="ok"){
        this.alerts.showSuccess('Paciente eliminado','Hecho');
        location.reload();
      }else{
        this.alerts.showError(response.result.error_msg,"Error");
      }
    });
  }

  return(){
    this.router.navigate(['dashboard', this.userid]);
  }

  getToken(){
    return localStorage.getItem('token');
  }

}
