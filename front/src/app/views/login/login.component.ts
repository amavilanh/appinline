import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {ApiService} from '../../services/api/api.service';
import {LoginI} from '../../models/login.interface';
import { ResponseI } from '../../models/response.interface';
import {Router} from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  //Formulario que recibe los datos de autentificacion 
  loginForm= new FormGroup({
    usuario : new FormControl('', Validators.required),
    password : new FormControl('',Validators.required)
  })

  constructor(private api:ApiService, private router:Router){}

  errorStatus:boolean = false;
  errorMsj:any = "";

  ngOnInit():void{
    this.checksession();
  }

  checksession(){
    if(localStorage.getItem('token')){
      this.router.navigate(['dashboard']);
    }
  }


  onLogin(form:any){

    this.api.toLogin(form).subscribe(data =>{
      //console.log(data);
      let dataResponse:ResponseI = data;
      if(dataResponse.status == "ok"){
        let id:number = dataResponse.result.userid;
        localStorage.setItem('token', dataResponse.result.token);
        this.router.navigate(['dashboard', id]);
      }else{
        this.errorStatus=true;
        this.errorMsj = dataResponse.result.error_msg;
      }
    });
    
  }


}
