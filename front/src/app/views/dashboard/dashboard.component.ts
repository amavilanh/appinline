import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import {ApiService} from '../../services/api/api.service';
import { UserI } from 'src/app/models/user.interface';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {

  userData:UserI;
  admin:Boolean=false;
  userid:any;

  constructor(private api:ApiService, private router:Router, private activerouter:ActivatedRoute){}

  ngOnInit(){
    this.userid = this.activerouter.snapshot.paramMap.get('id');
    this.api.getUser(this.userid).subscribe(data=>{
      this.userData=data[0];
      if(this.userData.Admin){
        this.admin=true;
      }else{
        this.admin=false;
      }
    });

    
  }

  salir(){
    localStorage.clear();
    this.router.navigate(['login']);
  }

  administrar(){
    this.router.navigate(['admin',this.userid]);
  }

  
}
