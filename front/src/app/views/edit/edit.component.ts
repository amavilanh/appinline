import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {  UserI } from '../../models/user.interface';
import { ResponseI } from '../../models/response.interface';
import { AlertService } from '../../services/alert/alert.service'
import { ApiService } from '../../services/api/api.service';
import  { FormGroup, FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  constructor(private activerouter:ActivatedRoute, private router:Router, private api:ApiService, private alerts:AlertService){}

  userData:UserI;
  formEdit = new FormGroup<any>({
    usuarioid: new FormControl({value: '', disabled: true}, Validators.required),
    nombre: new FormControl('', Validators.required),
    correo: new FormControl('', Validators.required),
    nacionalidad: new FormControl('', Validators.required),
    ciudad: new FormControl('', Validators.required),
    genero: new FormControl('', Validators.required),
    telefono: new FormControl('', Validators.required),
    fechanacimiento: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
    estado: new FormControl('', Validators.required),
    token: new  FormControl('', Validators.required)
  });

  ngOnInit(): void {
    let userid = this.activerouter.snapshot.paramMap.get('id');
    let token = this.getToken();
    this.api.getUser(userid).subscribe(data=>{
      this.userData = data[0];
      this.formEdit.setValue({
        'usuarioid': this.userData.UserId,
        'nombre': this.userData.Nombre,
        'correo': this.userData.Correo,
        'nacionalidad': this.userData.Nacionalidad,
        'ciudad': this.userData.Ciudad,
        'genero': this.userData.Genero,
        'telefono': this.userData.Celular,
        'fechanacimiento': this.userData.Fechanacimiento,
        'password': "123456789",
        'estado': this.userData.Estado,
        'token': token
      });
      
    })
  }

  getToken(){
    return localStorage.getItem('token');
  }

  postForm(form:UserI){
    this.api.putUser(form).subscribe(data=>{
      //console.log(data);
      let response:ResponseI=data;
      if(response.status=="ok"){
        this.alerts.showSuccess('Datos modificados','Hecho');
      }else{
        this.alerts.showError(response.result.error_msg,"Error");
      }
    })
  }

  return(){
    this.router.navigate(['admin']);
  }

}
