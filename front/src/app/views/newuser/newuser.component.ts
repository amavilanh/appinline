import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ResponseI } from '../../models/response.interface';
import { AlertService } from '../../services/alert/alert.service'
import { ApiService } from '../../services/api/api.service';
import  { FormGroup, FormControl, Validators } from '@angular/forms';
import {  UserI } from '../../models/user.interface';


@Component({
  selector: 'app-newuser',
  templateUrl: './newuser.component.html',
  styleUrls: ['./newuser.component.css']
})
export class NewuserComponent implements OnInit {

  constructor(private activerouter:ActivatedRoute, private router:Router, private api:ApiService, private alerts:AlertService){}

  formNew = new FormGroup<any>({
    nombre: new FormControl('', Validators.required),
    correo: new FormControl('', Validators.required),
    nacionalidad: new FormControl('', Validators.required),
    ciudad: new FormControl('', Validators.required),
    genero: new FormControl('', Validators.required),
    telefono: new FormControl('', Validators.required),
    fechanacimiento: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
    estado: new FormControl('', Validators.required),
    token: new  FormControl('', Validators.required)
  });

  ngOnInit(): void {
    let token = this.getToken();
    this.formNew.patchValue({
      'token':token
    });
  }


  postForm(form:UserI){
    //console.log(form);
    this.api.postUser(form).subscribe(data=>{
      console.log(data);
    })
  
  }

  return(){
    this.router.navigate(['admin']);
  }

  getToken(){
    return localStorage.getItem('token');
  }


}
