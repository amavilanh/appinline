import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './views/login/login.component';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { AdminComponent } from './views/admin/admin.component';
import { EditComponent } from './views/edit/edit.component';
import { SigninComponent} from './views/signin/signin.component';
import { NewuserComponent } from './views/newuser/newuser.component';

const routes: Routes = [
  {path:'' , redirectTo:'login' , pathMatch:'full'},
  {path:'login' , component:LoginComponent},
  {path:'dashboard/:id', component:DashboardComponent},
  {path:'admin/:id', component:AdminComponent},
  {path:'edit/:id', component:EditComponent},
  {path:'signin', component:SigninComponent},
  {path:'newuser', component:NewuserComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents= [LoginComponent, DashboardComponent, AdminComponent, EditComponent, SigninComponent, NewuserComponent]